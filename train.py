import torch
import torch.optim as optim
from tensorboardX import SummaryWriter
import numpy as np
import os
import argparse
import time
import matplotlib; matplotlib.use('Agg')
from im2mesh import config, data
from im2mesh.checkpoints import CheckpointIO
from im2mesh.optim import SWA
from im2mesh.encoder.autoencoder import extract_uresnet18


# Arguments
parser = argparse.ArgumentParser(
    description='Train a 3D reconstruction model.'
)
parser.add_argument('config', type=str, help='Path to config file.')
parser.add_argument('--no-cuda', action='store_true', help='Do not use cuda.')
parser.add_argument('--cuda-device', type=int, default=0, help='Cuda device id')
parser.add_argument('--exit-after', type=int, default=-1,
                    help='Checkpoint and exit after specified number of seconds'
                         'with exit code 2.')

args = parser.parse_args()
cfg = config.load_config(args.config, 'configs/default.yaml')
is_cuda = (torch.cuda.is_available() and not args.no_cuda)
device = torch.device("cuda:{}".format(args.cuda_device) if is_cuda else "cpu")

# Set t0
t0 = time.time()

# Shorthands
out_dir = cfg['training']['out_dir']
batch_size = cfg['training']['batch_size']
backup_every = cfg['training']['backup_every']
use_swa = cfg['training']['use_swa']
exit_after = args.exit_after

model_selection_metric = cfg['training']['model_selection_metric']
if cfg['training']['model_selection_mode'] == 'maximize':
    model_selection_sign = 1
elif cfg['training']['model_selection_mode'] == 'minimize':
    model_selection_sign = -1
else:
    raise ValueError('model_selection_mode must be '
                     'either maximize or minimize.')

# Output directory
if not os.path.exists(out_dir):
    os.makedirs(out_dir)

logger = SummaryWriter(os.path.join(out_dir, 'logs'))

# Dataset
train_dataset = config.get_dataset('train', cfg)
val_dataset = config.get_dataset('val', cfg)

train_loader = torch.utils.data.DataLoader(
    train_dataset, batch_size=batch_size, num_workers=4, shuffle=True,
    collate_fn=data.collate_remove_none,
    worker_init_fn=data.worker_init_fn)

val_loader = torch.utils.data.DataLoader(
    val_dataset, batch_size=5, num_workers=2, shuffle=False,
    collate_fn=data.collate_remove_none,
    worker_init_fn=data.worker_init_fn)

if cfg['training']['validate_choy']:
    val_choy2016_dataset = config.get_dataset('val_choy2016', cfg)
    val_choy2016_loader = torch.utils.data.DataLoader(
        val_choy2016_dataset, batch_size=10, num_workers=2, shuffle=False,
        collate_fn=data.collate_remove_none,
        worker_init_fn=data.worker_init_fn)

# For visualizations
vis_loader = torch.utils.data.DataLoader(
    val_dataset, batch_size=12, shuffle=True,
    collate_fn=data.collate_remove_none,
    worker_init_fn=data.worker_init_fn)
data_vis = next(iter(vis_loader))

# Model
model = config.get_model(cfg, device=device, dataset=train_dataset)
optimizer = config.get_optimizer(model, cfg)
checkpoint_io = CheckpointIO(out_dir, model=model, optimizer=optimizer)

if use_swa:
    swa_start = cfg['training']['swa_start']
    swa_cycle = cfg['training']['swa_cycle']
    swa_model = config.get_model(cfg, device=device, dataset=train_dataset)
    swa = SWA(swa_model, swa_start, 0)
    checkpoint_io = CheckpointIO(out_dir, model=model, optimizer=optimizer, swa=swa)

try:
    load_dict = checkpoint_io.load('model.pt', map_location=device)
except FileExistsError:
    if cfg['training']['model_file'] is not None:
        load_dict = checkpoint_io.load(cfg['training']['model_file'], map_location=device)
    else:
        load_dict = dict()

# Load pretrained image encoder
# if cfg['method'] == 'onet' and cfg['model']['encoder_kwargs']:
#     kwargs = cfg['model']['encoder_kwargs']
#     if kwargs['encoder_pretrained'] == 'uresnet18':
#         model.encoder = extract_uresnet18(
#             cfg['model']['c_dim'], kwargs['encoder_path'], device
#         )
#     model.to(device)
    ## fix image encoder first
    # for param in model.encoder.features.parameters():
    #     param.requires_grad = False
    ## finetune w/ differential learning rate
    # optimizer = optim.SGD([{'params': model.encoder.parameters(), 'lr': 1e-6},
    #                        {'params': model.decoder.parameters()}
    #                        ], lr=1e-4, momentum=0.9, weight_decay=1e-4)

epoch_it = load_dict.get('epoch_it', -1)
it = load_dict.get('it', -1)
metric_val_best = load_dict.get(
    'loss_val_best', -model_selection_sign * np.inf)

# Hack, not use Adam from pretrained
if not cfg['training']['load_old_optim']:
    optimizer = config.get_optimizer(model, cfg)
    checkpoint_io = CheckpointIO(out_dir, model=model, optimizer=optimizer)
if not cfg['training']['swa_start'] > epoch_it:
    swa = SWA(swa_model, swa_start, 0)
# Intialize training
scheduler = optim.lr_scheduler.CosineAnnealingWarmRestarts(
    optimizer, swa_cycle, eta_min=float(cfg['training']['lr_min']))
trainer = config.get_trainer(model, optimizer, logger, cfg, device=device)

# Hack because of previous bug in code
# TODO: remove, because shouldn't be necessary
if metric_val_best == np.inf or metric_val_best == -np.inf:
    metric_val_best = -model_selection_sign * np.inf

print('Current best validation metric (%s): %.8f'
      % (model_selection_metric, metric_val_best))

# Shorthands
print_every = cfg['training']['print_every']
checkpoint_every = cfg['training']['checkpoint_every']
validate_every = cfg['training']['validate_every']
visualize_every = cfg['training']['visualize_every']

# Print model
nparameters = sum(p.numel() for p in model.parameters())
print(model)
print('Total number of parameters: %d' % nparameters)

while True:
    epoch_it += 1
    evals = dict()
    if use_swa:
        scheduler.step(epoch_it)
        logger.add_scalar('lr', scheduler.get_lr()[0], it)

    if use_swa and epoch_it > swa_start and epoch_it % swa_cycle == 0:
        print("Update SWA model at {}".format(epoch_it - 1))
        swa.update_average_model(model)
        trainer.model = swa.swa_model  # Replace model with swa_model and eval
        swa.fix_batchnorm(trainer, train_loader)
        eval_dict = trainer.evaluate(val_loader)
        print('Validation metric on SWA (%s): %.4f'
              % (model_selection_metric, eval_dict[model_selection_metric]))
        for k, v in eval_dict.items():
            evals.setdefault(k, {}).update({'swa': v})

        trainer.model = model

    for batch in train_loader:
        it += 1
        loss = trainer.train_step(batch)
        logger.add_scalar('train/loss', loss, it)

        # Print output
        if print_every > 0 and (it % print_every) == 0:
            print('[Epoch %02d] it=%03d, loss=%.4f'
                  % (epoch_it, it, loss))

        # Visualize output
        if visualize_every > 0 and (it % visualize_every) == 0:
            print('Visualizing')
            trainer.visualize(data_vis, it)

        # Save checkpoint
        if (checkpoint_every > 0 and (it % checkpoint_every) == 0):
            print('Saving checkpoint')
            checkpoint_io.save('model.pt', epoch_it=epoch_it, it=it,
                               loss_val_best=metric_val_best)

        # Backup if necessary
        if (backup_every > 0 and (it % backup_every) == 0):
            print('Backup checkpoint')
            checkpoint_io.save('model_%d.pt' % it, epoch_it=epoch_it, it=it,
                               loss_val_best=metric_val_best)
        # Run validation
        if validate_every > 0 and (it % validate_every) == 0:
            eval_dict = trainer.evaluate(val_loader)
            metric_val = eval_dict[model_selection_metric]
            print('Validation metric (%s): %.4f'
                  % (model_selection_metric, metric_val))
            for k, v in eval_dict.items():
                evals.setdefault(k, {}).update({'': v})

            if model_selection_sign * (metric_val - metric_val_best) > 0:
                metric_val_best = metric_val
                print('New best model (loss %.4f)' % metric_val_best)
                checkpoint_io.save('model_best.pt', epoch_it=epoch_it, it=it,
                                   loss_val_best=metric_val_best)

            # Run validation for Choy2016 if not already
            if cfg['training']['validate_choy']:
                eval_dict = trainer.evaluate(val_choy2016_loader)
                metric_val = eval_dict[model_selection_metric]
                print('Validation metric on Choy2016 (%s): %.4f'
                      % (model_selection_metric, metric_val))
                for k, v in eval_dict.items():
                    evals.setdefault(k, {}).update({'choy2016': v})

        if len(evals) != 0:
            for k, v in evals.items():
                logger.add_scalars('val/%s' % k, v, it)
            evals = dict()

        # Exit if necessary
        if exit_after > 0 and (time.time() - t0) >= exit_after:
            print('Time limit reached. Exiting.')
            checkpoint_io.save('model.pt', epoch_it=epoch_it, it=it,
                               loss_val_best=metric_val_best)
            # break
            exit(3)
    # break
