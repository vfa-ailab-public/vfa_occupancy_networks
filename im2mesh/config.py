import yaml
import albumentations as transforms
from albumentations.pytorch import ToTensor
from torch import optim
from im2mesh.optim import AdaBoundW
from im2mesh import data
from im2mesh import onet, r2n2, psgn, pix2mesh, dmc, encoder, ounet
from im2mesh import preprocess


method_dict = {
    'onet': onet,
    'r2n2': r2n2,
    'psgn': psgn,
    'pix2mesh': pix2mesh,
    'dmc': dmc,
    'autoencoder': encoder,
    'ounet': ounet,
}


# General config
def load_config(path, default_path=None):
    ''' Loads config file.

    Args:
        path (str): path to config file
        default_path (bool): whether to use default path
    '''
    # Load configuration from file itself
    with open(path, 'r') as f:
        cfg_special = yaml.load(f)

    # Check if we should inherit from a config
    inherit_from = cfg_special.get('inherit_from')

    # If yes, load this config first as default
    # If no, use the default_path
    if inherit_from is not None:
        cfg = load_config(inherit_from, default_path)
    elif default_path is not None:
        with open(default_path, 'r') as f:
            cfg = yaml.load(f)
    else:
        cfg = dict()

    # Include main configuration
    update_recursive(cfg, cfg_special)

    return cfg


def update_recursive(dict1, dict2):
    ''' Update two config dictionaries recursively.

    Args:
        dict1 (dict): first dictionary to be updated
        dict2 (dict): second dictionary which entries should be used

    '''
    for k, v in dict2.items():
        if k not in dict1:
            dict1[k] = dict()
        if isinstance(v, dict):
            update_recursive(dict1[k], v)
        else:
            dict1[k] = v


# Models
def get_model(cfg, device=None, dataset=None):
    ''' Returns the model instance.

    Args:
        cfg (dict): config dictionary
        device (device): pytorch device
        dataset (dataset): dataset
    '''
    method = cfg['method']
    model = method_dict[method].config.get_model(
        cfg, device=device, dataset=dataset)
    # if cfg['training']['parallel']:
    #     model = nn.DataParallel(model)
    return model


# Optimizer
def get_optimizer(model, cfg, find_lr=False):
    ''' Returns an optimizer

    Args:
        model (nn.Module): the model which is used
        cfg (dict): config dictionary
    '''
    optim_name = cfg['training']['optim'].lower()
    if find_lr:
        lr = float(cfg['training']['find_lr'][0])
    else:
        lr = float(cfg['training']['lr'])

    if optim_name == 'sgd':
        opt = optim.SGD(model.parameters(), lr=lr, momentum=0.9, weight_decay=1e-4)
    elif optim_name == 'adaboundw':
        opt = AdaBoundW(model.parameters(), lr=lr, final_lr=1.0, weight_decay=1e-4)
    elif optim_name == 'adam':
        opt = optim.Adam(model.parameters(), lr=lr)
    else:
        print("Optimizer type not found. Default to Adam.")
    return opt


# Trainer
def get_trainer(model, optimizer, logger, cfg, device):
    ''' Returns a trainer instance.

    Args:
        model (nn.Module): the model which is used
        optimizer (optimizer): pytorch optimizer
        logger (SummaryWriter): tensorboardX SummaryWriter
        cfg (dict): config dictionary
        device (device): pytorch device
    '''
    method = cfg['method']
    trainer = method_dict[method].config.get_trainer(
        model, optimizer, logger, cfg, device)
    return trainer


# Generator for final mesh extraction
def get_generator(model, cfg, device):
    ''' Returns a generator instance.

    Args:
        model (nn.Module): the model which is used
        cfg (dict): config dictionary
        device (device): pytorch device
    '''
    method = cfg['method']
    generator = method_dict[method].config.get_generator(model, cfg, device)
    return generator


# Datasets
def get_dataset(mode, cfg, return_idx=False, return_category=False):
    ''' Returns the dataset.

    Args:
        model (nn.Module): the model which is used
        cfg (dict): config dictionary
        return_idx (bool): whether to include an ID field
    '''
    method = cfg['method']
    dataset_type = cfg['data']['dataset']
    dataset_folder = cfg['data']['path']
    categories = cfg['data']['classes']
    img_size = cfg['data']['img_size']
    # Get split
    splits = {
        'train': cfg['data']['train_split'],
        'val': cfg['data']['val_split'],
        'test': cfg['data']['test_split'],
    }

    mode_prefix = mode.split("_")[0]  # Split for val_choy2016
    split = splits[mode_prefix]

    # Create dataset
    if dataset_type == 'Shapes3D':
        # Dataset fields
        # Method specific fields (usually correspond to output)
        fields = method_dict[method].config.get_data_fields(mode_prefix, cfg)
        # Input fields
        inputs_field = method_dict[method].config.get_inputs_field(mode, cfg)
        if inputs_field is not None:
            fields['inputs'] = inputs_field

        if return_idx:
            fields['idx'] = data.IndexField()

        if return_category:
            fields['category'] = data.CategoryField()

        dataset = data.Shapes3dDataset(
            dataset_folder, fields,
            split=split,
            categories=categories,
        )
    elif dataset_type == 'Pix3D':
        tfms = [transforms.Resize(img_size, img_size)]
        if mode == 'train' and cfg['data']['img_augment']:
            tfms += [
                transforms.RandomBrightnessContrast(),
                transforms.GaussNoise(),
                transforms.HorizontalFlip(),
            ]
        tfms += [ToTensor()]
        transform = transforms.Compose(tfms)
        dataset = data.Pix3dDataset(
            dataset_folder, img_size, split=split,
            transform=transform, categories=categories,
        )
    elif dataset_type == 'kitti':
        dataset = data.KittiDataset(
            dataset_folder, img_size=cfg['data']['img_size'],
            return_idx=return_idx
        )
    elif dataset_type == 'online_products':
        dataset = data.OnlineProductDataset(
            dataset_folder, img_size=cfg['data']['img_size'],
            classes=cfg['data']['classes'],
            max_number_imgs=cfg['generation']['max_number_imgs'],
            return_idx=return_idx, return_category=return_category
        )
    elif dataset_type == 'images':
        dataset = data.ImageDataset(
            dataset_folder, img_size=cfg['data']['img_size'],
            return_idx=return_idx
        )
    elif dataset_type == 'multi_images':
        n_views = cfg['generation']['n_views_per_obj']
        folder_name = cfg['data']['img_folder']
        dataset = data.MultiImageDataset(
            dataset_folder, folder_name, 'test', categories=categories, n_views=n_views,
        )
    else:
        raise ValueError('Invalid dataset "%s"' % cfg['data']['dataset'])

    return dataset


def get_preprocessor(cfg, dataset=None, device=None):
    ''' Returns preprocessor instance.

    Args:
        cfg (dict): config dictionary
        dataset (dataset): dataset
        device (device): pytorch device
    '''
    p_type = cfg['preprocessor']['type']
    cfg_path = cfg['preprocessor']['config']
    model_file = cfg['preprocessor']['model_file']

    if p_type == 'psgn':
        preprocessor = preprocess.PSGNPreprocessor(
            cfg_path=cfg_path,
            pointcloud_n=cfg['data']['pointcloud_n'],
            dataset=dataset,
            device=device,
            model_file=model_file,
        )
    elif p_type is None:
        preprocessor = None
    else:
        raise ValueError('Invalid Preprocessor %s' % p_type)

    return preprocessor
