import os
from torchvision import models
from im2mesh.encoder import training, autoencoder


arch_dict = {
    'resnet18': models.resnet18,
    'aeresnet18': autoencoder.AEResnet18,
    'uresnet18': autoencoder.uresnet18,
    'resnet34': models.resnet34,
    'resnet50': models.resnet50,
    'resnet101': models.resnet101,
}

def get_model(cfg, device=None, dataset=None, **kwargs):
    ''' Return the image encoder model.

    Args:
        cfg (dict): imported yaml config
        device (device): pytorch device
        dataset (dataset): dataset
    '''
    encoder = cfg['model']['encoder']
    kwargs['c_dim'] = cfg['model']['c_dim']
    return arch_dict[encoder](**kwargs).to(device)


def get_trainer(model, optimizer, logger, cfg, device, **kwargs):
    ''' Returns the trainer object.

    Args:
        model (nn.Module): the Occupancy Network model
        optimizer (optimizer): pytorch optimizer object
        cfg (dict): imported yaml config
        device (device): pytorch device
    '''
    out_dir = cfg['training']['out_dir']
    vis_dir = os.path.join(out_dir, 'vis')

    trainer = training.Trainer(
        model, optimizer, logger,
        device=device, vis_dir=vis_dir,
        eval_sample=cfg['training']['eval_sample'],
    )

    return trainer


def get_data_fields(mode, cfg):
    return {}
