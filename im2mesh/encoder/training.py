import os
from tqdm import trange
import torch
from torchvision.transforms.functional import to_pil_image
from torch.nn import functional as F
from im2mesh.utils import visualize as vis
from im2mesh.training import BaseTrainer


class Trainer(BaseTrainer):
    ''' Trainer object for the Image Encoder

    Args:
        model (nn.Module): Occupancy Network model
        optimizer (optimizer): pytorch optimizer object
        logger (SummaryWriter): tensorboardX summary writer
        device (device): pytorch device
        vis_dir (str): visualization directory
        eval_sample (bool): whether to evaluate samples

    '''

    def __init__(self, model, optimizer, logger=None, device=None,
                 vis_dir=None, eval_sample=False):
        self.model = model
        self.optimizer = optimizer
        self.logger = logger
        self.device = device
        self.vis_dir = vis_dir
        self.eval_sample = eval_sample

        if vis_dir is not None and not os.path.exists(vis_dir):
            os.makedirs(vis_dir)

    def train_step(self, data):
        ''' Performs a training step.
        Args:
            data (dict): data dictionary
        '''
        self.model.train()
        self.optimizer.zero_grad()
        loss = self.compute_loss(data)
        loss.backward()
        self.optimizer.step()
        return loss.item()

    def eval_step(self, data):
        ''' Performs an evaluation step.

        Args:
            data (dict): data dictionary
        '''
        self.model.eval()
        eval_dict = {}
        with torch.no_grad():
            loss = self.compute_loss(data)
        eval_dict['loss'] = loss.item()

        return eval_dict

    def visualize(self, data, it=0, tb=True):
        ''' Performs a visualization step for the data.

        Args:
            data (dict): data dictionary
            tb (bool): also visualize in tensorboard
        '''
        device = self.device

        batch_size = data['inputs'].size(0)
        inputs = data.get('inputs', torch.empty(batch_size, 0)).to(device)
        gts = data.get('inputs.gt')

        kwargs = {}
        with torch.no_grad():
            outputs = torch.sigmoid(self.model(inputs, **kwargs))

        for i in trange(batch_size):
            input_img_path = os.path.join(self.vis_dir, '%03d_in.png' % i)
            output_img_path = os.path.join(self.vis_dir, '%03d_out.png' % i)
            gt_img_path = os.path.join(self.vis_dir, '%03d_gt.png' % i)
            inp = inputs[i].cpu()
            out = outputs[i].cpu()
            gt = gts[i].cpu()
            vis.visualize_data(inp, 'img', input_img_path)
            vis.visualize_data(out, 'img', output_img_path)
            vis.visualize_data(gt, 'img', gt_img_path)
            if self.logger and tb:
                self.logger.add_image('{:03d}/in'.format(i), inp, it)
                self.logger.add_image('{:03d}/out'.format(i), out, it)
                self.logger.add_image('{:03d}/gt'.format(i), gt, it)

    def compute_loss(self, data):
        ''' Computes the loss.

        Args:
            data (dict): data dictionary
        '''
        device = self.device
        gt = data.get('inputs.gt')[:, 0:1, :, :].to(device)
        inputs = data.get('inputs', torch.empty(gt.size(0), 0)).to(device)

        outputs = self.model(inputs)

        return F.binary_cross_entropy_with_logits(outputs, gt)
