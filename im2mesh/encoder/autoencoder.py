import math
import torch
import torch.nn as nn
from torchvision import models
from torchvision.models import resnet
from fastai.vision.models import DynamicUnet
from fastai.vision.learner import create_body

from im2mesh.encoder import conv
from im2mesh.common import normalize_imagenet


def conv_transpose3x3(in_planes, out_planes, stride=1, output_padding=0):
    """3x3 transpose convolution with padding"""
    return nn.ConvTranspose2d(in_planes, out_planes, kernel_size=3, stride=stride,
                              padding=1, output_padding=output_padding, bias=False)


class BasicUpBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None):
        super(BasicUpBlock, self).__init__()
        if stride != 1:
            self.conv1 = conv_transpose3x3(inplanes, planes, stride, output_padding=1)
        else:
            self.conv1 = conv_transpose3x3(inplanes, planes, stride)
        self.bn1 = nn.BatchNorm2d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv_transpose3x3(planes, planes)
        self.bn2 = nn.BatchNorm2d(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out


class AEResnet18(nn.Module):
    r''' ResNet-18 auto-encoder network for image input and reconstruction.
    Args:
        c_dim (int): output dimension of the latent embedding
        normalize (bool): whether the input images should be normalized
        use_linear (bool): whether a final linear layer should be used
    '''

    def __init__(self, c_dim, normalize=True, block=BasicUpBlock):
        super().__init__()
        self.c_dim = c_dim
        self.normalize = normalize
        self.encoder = models.resnet18(pretrained=True)
        # self.encoder.fc = nn.Linear(512, c_dim)
        self.encoder.avgpool = nn.Sequential()
        self.encoder.fc = nn.Sequential()

        self.inplanes = 512
        self.decoder = nn.Sequential(
            self._make_layer(block, 256, 2, stride=2),
            self._make_layer(block, 128, 2, stride=2),
            self._make_layer(block, 64, 2, stride=2),
            self._make_layer(block, 64, 1, stride=2),
            nn.ConvTranspose2d(self.inplanes, 1, kernel_size=7, stride=2,
                               padding=3, output_padding=1, bias=False),
        )

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1):
        upsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            upsample = nn.Sequential(
                nn.ConvTranspose2d(self.inplanes, planes * block.expansion,
                                   kernel_size=1, stride=stride, output_padding=1,
                                   bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, upsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def forward(self, x):
        if self.normalize:
            x = normalize_imagenet(x)
        emb = self.encoder(x)
        emb = emb.view(emb.shape[0], 512, 7, 7)
        rec = self.decoder(emb)
        return rec

def extract_AEResnet18(model_path=None):
    pass

def uresnet18(c_dim=256, **kwargs):
    body = create_body(models.resnet18, pretrained=True)
    unet = DynamicUnet(body, 1)
    return unet

def extract_uresnet18(c_dim, encoder_path=None, device='cpu'):
    uresnet = uresnet18()
    if encoder_path:
        uresnet.load_state_dict(torch.load(encoder_path, map_location=device).get('model'))

    encoder = uresnet[0]
    return conv.Resnet18(c_dim, encoder)
