import albumentations as transforms
from albumentations.torch import ToTensor
import torch
import torch.distributions as dist
from torch import nn
import os
from im2mesh.ounet import models, training, generation
from im2mesh import data
from im2mesh import config


def get_model(cfg, device=None, dataset=None, **kwargs):
    ''' Return the Occupancy Network model.

    Args:
        cfg (dict): imported yaml config
        device (device): pytorch device
        dataset (dataset): dataset
    '''
    decoder = cfg['model']['decoder']
    encoder = cfg['model']['encoder']
    dim = cfg['data']['dim']
    z_dim = cfg['model']['z_dim']
    c_dim = cfg['model']['c_dim']
    decoder_kwargs = cfg['model']['decoder_kwargs']

    decoder = models.decoder_dict[decoder](
        dim=dim, z_dim=z_dim, c_dim=c_dim,
        **decoder_kwargs
    )

    if encoder == 'idx':
        encoder = nn.Embedding(len(dataset), c_dim)
    elif encoder is not None:
        encoder = encoder
    else:
        encoder = None

    p0_z = get_prior_z(cfg, device)
    model = models.OccupancyUNetwork(
        decoder, encoder, p0_z, c_dim, device=device
    )

    return model


def get_trainer(model, optimizer, logger, cfg, device, **kwargs):
    ''' Returns the trainer object.

    Args:
        model (nn.Module): the Occupancy Network model
        optimizer (optimizer): pytorch optimizer object
        cfg (dict): imported yaml config
        device (device): pytorch device
    '''
    threshold = cfg['test']['threshold']
    out_dir = cfg['training']['out_dir']
    vis_dir = os.path.join(out_dir, 'vis')
    input_type = cfg['data']['input_type']

    trainer = training.Trainer(
        model, optimizer, logger,
        device=device, input_type=input_type,
        vis_dir=vis_dir, threshold=threshold,
        eval_sample=cfg['training']['eval_sample'],
    )

    return trainer


def get_generator(model, cfg, device, **kwargs):
    ''' Returns the generator object.

    Args:
        model (nn.Module): Occupancy Network model
        cfg (dict): imported yaml config
        device (device): pytorch device
    '''
    preprocessor = config.get_preprocessor(cfg, device=device)

    generator = generation.Generator3D(
        model,
        device=device,
        threshold=cfg['test']['threshold'],
        resolution0=cfg['generation']['resolution_0'],
        upsampling_steps=cfg['generation']['upsampling_steps'],
        sample=cfg['generation']['use_sampling'],
        refinement_step=cfg['generation']['refinement_step'],
        simplify_nfaces=cfg['generation']['simplify_nfaces'],
        preprocessor=preprocessor,
    )
    return generator


def get_prior_z(cfg, device, **kwargs):
    ''' Returns prior distribution for latent code z.

    Args:
        cfg (dict): imported yaml config
        device (device): pytorch device
    '''
    z_dim = cfg['model']['z_dim']
    p0_z = dist.Normal(
        torch.zeros(z_dim, device=device),
        torch.ones(z_dim, device=device)
    )

    return p0_z


def get_data_fields(mode, cfg):
    ''' Returns the data fields.

    Args:
        mode (str): the mode which is used
        cfg (dict): imported yaml config
    '''
    points_transform = data.SubsamplePoints(cfg['data']['points_subsample'])
    with_transforms = cfg['model']['use_camera']

    fields = {}
    fields['points'] = data.PointsField(
        cfg['data']['points_file'], points_transform,
        with_transforms=with_transforms,
        unpackbits=cfg['data']['points_unpackbits'],
    )

    if mode in ('val', 'test'):
        points_iou_file = cfg['data']['points_iou_file']
        voxels_file = cfg['data']['voxels_file']
        if points_iou_file is not None:
            fields['points_iou'] = data.PointsField(
                points_iou_file,
                with_transforms=with_transforms,
                unpackbits=cfg['data']['points_unpackbits'],
            )
        if voxels_file is not None:
            fields['voxels'] = data.VoxelsField(voxels_file)

    return fields


def get_inputs_field(mode, cfg):
    ''' Returns the inputs fields.

    Args:
        mode (str): the mode which is used
        cfg (dict): config dictionary
    '''
    input_type = cfg['data']['input_type']
    img_size = cfg['data']['img_size']
    img_folder = cfg['data']['img_folder']
    img_ext = cfg['data']['img_ext']
    with_transforms = cfg['data']['with_transforms']
    with_gt = cfg['data']['with_gt']
    bg_augment = float(cfg['data']['bg_augment'])
    scale_limit = float(cfg['data']['scale_limit'])

    if bg_augment:
        bg_augment = data.transforms.InsertBackground(
            cfg['data']['bg_folder'], img_size, scale_limit, p=bg_augment,
        )
    else:
        bg_augment = None
    tfms = []
    if mode == 'train' and cfg['data']['img_augment']:
        tfms += [
            transforms.RandomBrightnessContrast(),
            transforms.HueSaturationValue(),
            # transforms.GaussianBlur(),
            transforms.GaussNoise()
        ]
    else:
        tfms += [
            transforms.Resize(img_size, img_size)
        ]
    tfms += [ToTensor()]
    transform = transforms.Compose(tfms)

    with_camera = cfg['data']['img_with_camera']

    if mode == 'train':
        random_view = True
    else:
        random_view = False

    inputs_field = data.ImagesField(
        img_folder, 'depth_phuc2019', img_size,
        bg_augment, transform,
        extension=img_ext, random_view=random_view,
        with_camera=with_camera, with_gt=with_gt,
    )
    return inputs_field
