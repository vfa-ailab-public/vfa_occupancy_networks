import numpy as np

from PIL import Image
from pathlib import Path
from albumentations.core.transforms_interface import ImageOnlyTransform


# Transforms
class PointcloudNoise(object):
    ''' Point cloud noise transformation class.

    It adds noise to point cloud data.

    Args:
        stddev (int): standard deviation
    '''

    def __init__(self, stddev):
        self.stddev = stddev

    def __call__(self, data):
        ''' Calls the transformation.

        Args:
            data (dictionary): data dictionary
        '''
        data_out = data.copy()
        points = data[None]
        noise = self.stddev * np.random.randn(*points.shape)
        noise = noise.astype(np.float32)
        data_out[None] = points + noise
        return data_out


class SubsamplePointcloud(object):
    ''' Point cloud subsampling transformation class.

    It subsamples the point cloud data.

    Args:
        N (int): number of points to be subsampled
    '''
    def __init__(self, N):
        self.N = N

    def __call__(self, data):
        ''' Calls the transformation.

        Args:
            data (dict): data dictionary
        '''
        data_out = data.copy()
        points = data[None]
        normals = data['normals']

        indices = np.random.randint(points.shape[0], size=self.N)
        data_out[None] = points[indices, :]
        data_out['normals'] = normals[indices, :]

        return data_out


class SubsamplePoints(object):
    ''' Points subsampling transformation class.

    It subsamples the points data.

    Args:
        N (int): number of points to be subsampled
    '''
    def __init__(self, N):
        self.N = N

    def __call__(self, data):
        ''' Calls the transformation.

        Args:
            data (dictionary): data dictionary
        '''
        points = data[None]
        occ = data['occ']

        data_out = data.copy()
        if isinstance(self.N, int):
            idx = np.random.randint(points.shape[0], size=self.N)
            data_out.update({
                None: points[idx, :],
                'occ':  occ[idx],
            })
        else:
            Nt_out, Nt_in = self.N
            occ_binary = (occ >= 0.5)
            points0 = points[~occ_binary]
            points1 = points[occ_binary]

            idx0 = np.random.randint(points0.shape[0], size=Nt_out)
            idx1 = np.random.randint(points1.shape[0], size=Nt_in)

            points0 = points0[idx0, :]
            points1 = points1[idx1, :]
            points = np.concatenate([points0, points1], axis=0)

            occ0 = np.zeros(Nt_out, dtype=np.float32)
            occ1 = np.ones(Nt_in, dtype=np.float32)
            occ = np.concatenate([occ0, occ1], axis=0)

            volume = occ_binary.sum() / len(occ_binary)
            volume = volume.astype(np.float32)

            data_out.update({
                None: points,
                'occ': occ,
                'volume': volume,
            })
        return data_out


class InsertBackground(object):
    '''Insert random background to PNG image with transparent layer.'''
    def __init__(self, folder_name, img_size=224, scale_limit=0.0, p=1.0, extension='jpg'):
        self.folder = Path(folder_name)
        self.img_size = img_size
        self.scale_limit = scale_limit
        self.img_list = list(self.folder.glob(f"**/*.{extension}"))
        self.len = len(self.img_list)
        self.p = p

    def __len__(self):
        return self.len

    def __call__(self, img):
        if np.random.random() > self.p:
            return img.resize((self.img_size, self.img_size))
        idx = np.random.randint(0, self.len)
        bg = Image.open(str(self.img_list[idx]))
        bg = bg.resize((self.img_size, self.img_size))
        # Scale 3D object
        offsets = [0, 0]
        scale = np.random.normal(1, self.scale_limit)
        new_size = int(self.img_size * scale)
        img = img.resize((new_size, new_size))
        offsets = np.random.randint(0, abs(self.img_size - new_size) + 1, 2).tolist()
        bg.paste(img, offsets, mask=img)
        return bg.convert('RGB')
