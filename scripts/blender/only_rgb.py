"""
# A simple script that uses blender to render views of a single object
# by rotation the camera around it. Also produces depth map at the same time.
#
# Example:
# blender --background --python mytest.py -- --views 10 /path/to/my.obj
#
"""
import argparse
import sys
import os
from glob import glob

import numpy as np
import bpy


def parse_args():
    parser = argparse.ArgumentParser(
        description='Renders given obj file by rotation a camera around it.'
    )
    parser.add_argument('--min_views', type=int, default=40,
                        help='number of minimum views to be rendered')
    parser.add_argument('--radius', type=float, default=1.,
                        help='radius of camera view icosahedron')
    parser.add_argument('obj', type=str,
                        help='Path to the obj file to be rendered.')
    parser.add_argument('--output_prefix', type=str, default='/tmp',
                        help='The prefix of the path the output will be dumped to.')
    parser.add_argument('--output_suffix', type=str, default='img_phuc2019',
                        help='The suffix of the path the output will be dumped to. prefix/model_id/suffix/')
    parser.add_argument('--num_lights', type=int, default=1,
                        help='How many random light to simulate per view.')
    parser.add_argument('--scale', type=float, default=1,
                        help='Scaling factor applied to model. Depends on mesh size.')
    parser.add_argument('--remove_doubles', type=bool, default=True,
                        help='Remove double vertices to improve mesh quality.')
    parser.add_argument('--edge_split', type=bool, default=True,
                        help='Adds edge split filter.')
    parser.add_argument('--depth_scale', type=float, default=1.4,
                        help=('Scaling that is applied to depth. Depends on mesh size.'
                              'Try out various values until you get a good result.'
                              'Ignored if format is OPEN_EXR.'))
    parser.add_argument('--color_depth', type=str, default='8',
                        help='Number of bit per channel used for output. 8 or 16.')
    parser.add_argument('--format', type=str, default='PNG',
                        help='Format of files generated. Either PNG or OPEN_EXR')
    argv = sys.argv[sys.argv.index("--") + 1:]
    args = parser.parse_args(argv)
    return args


def hinter_sampling(min_n_pts, radius=2):
    '''
    Sphere sampling based on refining icosahedron as described in:
    Hinterstoisser et al., Simultaneous Recognition and Homography Extraction of
    Local Patches with a Simple Linear Classifier, BMVC 2008

    :param min_n_pts: Minimum required number of points on the whole view sphere.
    :param radius: Radius of the view sphere.
    :return: 3D points on the sphere surface and a list that indicates on which
             refinement level the points were created.
    '''

    # Get vertices and faces of icosahedron
    a, b, c = 0.0, 1.0, (1.0 + np.sqrt(5.0)) / 2.0
    pts = [(-b, c, a), (b, c, a), (-b, -c, a), (b, -c, a), (a, -b, c), (a, b, c),
           (a, -b, -c), (a, b, -c), (c, a, -b), (c, a, b), (-c, a, -b), (-c, a, b)]
    faces = [(0, 11, 5), (0, 5, 1), (0, 1, 7), (0, 7, 10), (0, 10, 11), (1, 5, 9),
             (5, 11, 4), (11, 10, 2), (10, 7, 6), (7, 1, 8), (3, 9, 4), (3, 4, 2),
             (3, 2, 6), (3, 6, 8), (3, 8, 9), (4, 9, 5), (2, 4, 11), (6, 2, 10),
             (8, 6, 7), (9, 8, 1)]

    # Refinement level on which the points were created
    pts_level = [0 for _ in range(len(pts))]

    ref_level = 0
    while len(pts) < min_n_pts:
        ref_level += 1
        edge_pt_map = {}  # Mapping from an edge to a newly added point on that edge
        faces_new = []  # New set of faces

        # Each face is replaced by 4 new smaller faces
        for face in faces:
            pt_inds = list(face)  # List of point IDs involved in the new faces
            for i in range(3):
                # Add a new point if this edge hasn't been processed yet,
                # or get ID of the already added point.
                edge = (face[i], face[(i + 1) % 3])
                edge = (min(edge), max(edge))
                if edge not in edge_pt_map.keys():
                    pt_new_id = len(pts)
                    edge_pt_map[edge] = pt_new_id
                    pt_inds.append(pt_new_id)

                    pt_new = 0.5 * (np.array(pts[edge[0]]) + np.array(pts[edge[1]]))
                    pts.append(pt_new.tolist())
                    pts_level.append(ref_level)
                else:
                    pt_inds.append(edge_pt_map[edge])

            # Replace the current face with 4 new faces
            faces_new += [(pt_inds[0], pt_inds[3], pt_inds[5]),
                          (pt_inds[3], pt_inds[1], pt_inds[4]),
                          (pt_inds[3], pt_inds[4], pt_inds[5]),
                          (pt_inds[5], pt_inds[4], pt_inds[2])]
        faces = faces_new

    # Project the points to a sphere
    pts = np.array(pts)
    pts *= np.reshape(radius / np.linalg.norm(pts, axis=1), (pts.shape[0], 1))

    # Collect point connections
    pt_conns = {}
    for face in faces:
        for i in range(len(face)):
            pt_conns.setdefault(face[i], set()).add(face[(i + 1) % len(face)])
            pt_conns[face[i]].add(face[(i + 2) % len(face)])

    # Order the points - starting from the top one and adding the connected points
    # sorted by azimuth
    top_pt_id = np.argmax(pts[:, 2])
    pts_ordered = []
    pts_todo = [top_pt_id]
    pts_done = [False for _ in range(pts.shape[0])]

    def calc_azimuth(x, y):
        two_pi = 2.0 * np.pi
        return (np.arctan2(y, x) + two_pi) % two_pi

    while len(pts_ordered) != pts.shape[0]:
        # Sort by azimuth
        pts_todo = sorted(pts_todo, key=lambda i: calc_azimuth(pts[i][0], pts[i][1]))
        pts_todo_new = []
        for pt_id in pts_todo:
            pts_ordered.append(pt_id)
            pts_done[pt_id] = True
            pts_todo_new += [i for i in pt_conns[pt_id]]  # Find the connected points

        # Points to be processed in the next iteration
        pts_todo = [i for i in set(pts_todo_new) if not pts_done[i]]

    # Re-order the points and faces
    pts = pts[np.array(pts_ordered), :]
    pts_level = [pts_level[i] for i in pts_ordered]
    pts_order = np.zeros((pts.shape[0],))
    pts_order[np.array(pts_ordered)] = np.arange(pts.shape[0])
    # for face_id in range(len(faces)):
    #     faces[face_id] = [pts_order[i] for i in faces[face_id]]

    # remove too low angles
    pts = pts[pts[:, 2] >= -0.5]

    return pts


def load_obj(path):
    # Delete default cube
    bpy.data.objects['Cube'].select = True
    bpy.ops.object.delete()
    # Load object
    bpy.ops.import_scene.obj(filepath=path)
    for object in bpy.context.scene.objects:
        if object.name in ['Camera', 'Lamp']:
            continue
        bpy.context.scene.objects.active = object
        if args.scale != 1:
            bpy.ops.transform.resize(value=(args.scale , args.scale , args.scale))
            bpy.ops.object.transform_apply(scale=True)
        if args.remove_doubles:
            bpy.ops.object.mode_set(mode='EDIT')
            bpy.ops.mesh.remove_doubles()
            bpy.ops.object.mode_set(mode='OBJECT')
        if args.edge_split:
            bpy.ops.object.modifier_add(type='EDGE_SPLIT')
            bpy.context.object.modifiers["EdgeSplit"].split_angle = 1.32645
            bpy.ops.object.modifier_apply(apply_as='DATA', modifier="EdgeSplit")


def setup_scene(scene):
    scene.render.resolution_x = 512
    scene.render.resolution_y = 512
    scene.render.resolution_percentage = 100
    scene.render.alpha_mode = 'TRANSPARENT'
    scene.render.image_settings.file_format = 'PNG'  # set output format to .png


def parent_obj_to_camera(b_camera):
    origin = (0, 0, 0)
    b_empty = bpy.data.objects.new("Empty", None)
    b_empty.location = origin
    b_camera.parent = b_empty  # setup parenting

    scn = bpy.context.scene
    scn.objects.link(b_empty)
    scn.objects.active = b_empty
    return b_empty


def setup_cam(cam):
    b_empty = parent_obj_to_camera(cam)
    cam_constraint = cam.constraints.new(type='TRACK_TO')
    cam_constraint.track_axis = 'TRACK_NEGATIVE_Z'
    cam_constraint.up_axis = 'UP_Y'
    cam_constraint.target = b_empty


def make_light():
    lamp = bpy.data.lamps['Lamp']
    lamp.type = 'SUN'
    lamp.use_specular = True
    lamp.use_diffuse = True
    # Add another light source so stuff facing away from light not completely dark
    bpy.ops.object.lamp_add(type='SUN')
    lamp2 = bpy.data.lamps['Sun']
    lamp2.shadow_method = 'NOSHADOW'
    lamp2.use_specular = False
    lamp2.energy = 0.3
    bpy.data.objects['Sun'].rotation_euler = bpy.data.objects['Lamp'].rotation_euler
    bpy.data.objects['Sun'].rotation_euler[0] += 180


def random_light():
    euler = np.deg2rad(np.random.normal(45, 30, 3))
    bpy.data.objects['Lamp'].rotation_euler = euler
    bpy.data.objects['Sun'].rotation_euler = bpy.data.objects['Lamp'].rotation_euler
    bpy.data.objects['Sun'].rotation_euler[0] += 180


def random_reflection():
    intensity = np.random.uniform(0.5, 1)
    color = np.random.normal(intensity, 0.2, 3)
    bpy.data.materials['Material'].diffuse_color = color
    for material in bpy.data.materials:
        material.diffuse_color = color
        material.specular_color = color

# do the render
args = parse_args()

model_identifier = os.path.split(os.path.split(args.obj)[0])[1]
fp = os.path.join(
    args.output_prefix, model_identifier, args.output_suffix, "clean"
)
# Skip if done
if len(glob(fp + "*.png")) == 132:
    print("Skip: " + fp)
    exit()

# redirect output to log file
logfile = 'blender_render.log'
open(logfile, 'a').close()
old = os.dup(1)
sys.stdout.flush()
os.close(1)
os.open(logfile, os.O_WRONLY)

load_obj(args.obj)
# Setup Scene
scene = bpy.context.scene
setup_scene(scene)
# Setup camera track origin
cam = scene.objects['Camera']
setup_cam(cam)
make_light()

cameras = hinter_sampling(args.min_views, args.radius)
for i, coord in enumerate(cameras):
    bpy.data.objects['Camera'].location = coord
    for l in range(args.num_lights):
        random_light()
        random_reflection()
        scene.render.filepath = fp + '_c{:03d}_l{:02d}'.format(int(i), int(l))
        bpy.ops.render.render(write_still=True)  # render still


# disable output redirection
os.close(1)
os.dup(old)
os.close(old)
