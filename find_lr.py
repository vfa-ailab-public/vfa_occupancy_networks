import torch
import torch.optim as optim
from tensorboardX import SummaryWriter
import numpy as np
import os
import argparse
import time
import matplotlib; matplotlib.use('Agg')
from im2mesh import config, data
from im2mesh.checkpoints import CheckpointIO
from im2mesh.utils.lr_finder import LRFinder
from im2mesh.encoder.autoencoder import extract_uresnet18


# Arguments
parser = argparse.ArgumentParser(
    description='Find a LR for 3D reconstruction model.'
)
parser.add_argument('config', type=str, help='Path to config file.')
parser.add_argument('--no-cuda', action='store_true', help='Do not use cuda.')
parser.add_argument('--cuda-device', type=int, default=0, help='Cuda device id')

args = parser.parse_args()
cfg = config.load_config(args.config, 'configs/default.yaml')
is_cuda = (torch.cuda.is_available() and not args.no_cuda)
device = torch.device("cuda:{}".format(args.cuda_device) if is_cuda else "cpu")

# Set t0
t0 = time.time()

# Shorthands
out_dir = cfg['training']['out_dir']
batch_size = cfg['training']['batch_size']

if cfg['training']['model_selection_mode'] == 'maximize':
    model_selection_sign = 1
elif cfg['training']['model_selection_mode'] == 'minimize':
    model_selection_sign = -1
else:
    raise ValueError('model_selection_mode must be '
                     'either maximize or minimize.')

# Output directory
if not os.path.exists(out_dir):
    os.makedirs(out_dir)

logger = SummaryWriter(os.path.join(out_dir, 'logs'))

# Dataset
train_dataset = config.get_dataset('train', cfg)

train_loader = torch.utils.data.DataLoader(
    train_dataset, batch_size=batch_size, num_workers=2, shuffle=True,
    collate_fn=data.collate_remove_none,
    worker_init_fn=data.worker_init_fn)

# Model
model = config.get_model(cfg, device=device, dataset=train_dataset)
optimizer = config.get_optimizer(model, cfg)

checkpoint_io = CheckpointIO(out_dir, model=model, optimizer=optimizer)
try:
    load_dict = checkpoint_io.load('model.pt', map_location=device)
except FileExistsError:
    if cfg['training']['model_file'] is not None:
        load_dict = checkpoint_io.load(cfg['training']['model_file'])
    else:
        load_dict = dict()
epoch_it = load_dict.get('epoch_it', -1)
it = load_dict.get('it', -1)
metric_val_best = load_dict.get(
    'loss_val_best', -model_selection_sign * np.inf)

# Load pretrained image encoder
if cfg['method'] == 'onet' and cfg['model']['encoder_kwargs']:
    kwargs = cfg['model']['encoder_kwargs']
    if kwargs['encoder_pretrained'] == 'uresnet18':
        model.encoder = extract_uresnet18(
            cfg['model']['c_dim'], kwargs['encoder_path'], device
        )
    model.to(device)
    # fix image encoder first
    for param in model.encoder.features.parameters():
        param.requires_grad = False

# Print model
nparameters = sum(p.numel() for p in model.parameters())
print(model)
print('Total number of parameters: %d' % nparameters)

# Find LR
if not cfg['training']['find_lr']:
    print("LR ranges not found in config. Exit findding lr.")
    exit(1)
else:
    print('Finding learning rate:')
    lr_range = list(map(float, cfg['training']['find_lr']))

optimizer = config.get_optimizer(model, cfg, find_lr=True)
trainer = config.get_trainer(model, optimizer, logger, cfg, device=device)

lr_finder = LRFinder(trainer)
lr_finder.range_test(train_loader, end_lr=lr_range[1], num_iter=50)

fig = lr_finder.plot(skip_start=0, skip_end=2)
fig.savefig('{}/findLR_it{}.png'.format(out_dir, it))
# FIXME mysterious bug, TB save if execute in debugger :()
logger.add_figure('findLR', fig, it)

print('Finding LR complete. Exit.')
